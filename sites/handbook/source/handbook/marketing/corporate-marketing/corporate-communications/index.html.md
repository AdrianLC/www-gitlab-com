---
layout: handbook-page-toc
title: "Corporate Communications Handbook"
description: Objectives and Goals, Responsibilties, Contact Info and Resources for Corporate Communications at GitLab
twitter_image: 
twitter_image_alt: 
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Welcome to the Corporate Communications Handbook!

## Mission Statement

The mission of GitLab’s Corporate Communications team is to amplify GitLab's product, people and partner integrations in the media and via social media channels. This team is responsible for global public relations (PR), social media and internal communications. Corporate Communications Job families can be found [here](https://about.gitlab.com/job-families/marketing/corporate-communications-manager/)

**This page is the single source of truth for corporate communications objectives/goals, contact information, messaging, PR/social media guidelines, approval processes, strategy and more**.

## What We Do

The GitLab Corporate Communications team is responsibility for the following activities and communications channels:
- External corporate and product messaging
- Press releases
- Media relations (Print/Broadcast/Podcasts)
- Media sponsorships
- Contributed article placement
- Executive Communication and Speaking
- Awards
- [Incident Communications Plans](/handbook/marketing/corporate-marketing/incident-communications-plan/)
- Internal Communications
- [Social Media](/handbook/marketing/corporate-marketing/social-marketing/)

## Objectives and Goals

As detailed in GitLab’s public [CMO OKRs](/company/okrs/), GitLab’s corporate communications team seeks to elevate the profile of GitLab in the media and investor circles, positioning it as a pioneer of remote work, increasing share of voice against competitors, and pulling through key messages in feature articles.

- Leverage events to generate media interest in GitLab's people and products
- Form and foster relationships with key reporters and publications
- Position GitLab executives and subject matter experts as thought leaders in their areas of expertise
- Increase GitLab's presence in media awards and accolades
- Increase GitLab's contributed content
- Work with social media team to cross-promote and amplify GitLab's media inclusions

### Top Initiatives

#### OKRs

`more details to come`

#### Metrics + Results

`more details to come`

## Contacting GitLab's Corporate Communications team

##### External Parties
Please visit our [Get In Touch page](/press/#get-in-touch) for contact details.

##### GitLab Team Members 
Please use the `#external-comms` Slack channel and please follow the [Communication Activation Tree](https://docs.google.com/document/d/1qos3kjM_yIhS8-syey7WfR22SzmnHYFSCZpnFnBv7Rw/edit).

## How to Work With Us

### GitLab Master Messaging Document

GitLab's Corporate Communications (PR/Social Media) team is the [DRI](/handbook/people-group/directly-responsible-individuals/) (directly responsible individual) for the GitLab Master Messaging Document. If this document is needed, please request access in the `#external-comms` Slack channel.

### Communications trainings available to all team members
The communications team develops trainings and certifications for GitLab team members to build up their practices when representing the company with the media, at an event, or on social media. [Consider taking a communication training to learn more about Social Media 101 and How to Make Your Content Work Harder for You.](https://about.gitlab.com/handbook/marketing/corporate-marketing/corporate-communications-resourses-trainings/)

### Incident Communications Plans 

For incident communications plans, please check out [this handbook page](/handbook/marketing/corporate-marketing/incident-communications-plan/).

### Requests for External Announcements

This process allows us to decide on the best channel to communicate different types of updates and news to our users, customers, and community.

Please follow the instructions below to request a formalized announcement around any of the following:

- A new product feature and capabilities
- A partner integration
- A significant milestone achieved
- A new initiative
- A customer case study
- Inclusion in an analyst report
- A breaking change
- A deprecation
- A change in policy or pricing
- A product promotion (launching or ending)
- Something else? If you're not sure if your case applies, please follow the directions below anyway, so the team can assess how best to proceed.

Please submit a request via an `announcement` [issue template in the Corporate Marketing project](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=announcement) before opening an issue for a blog post, for example. In the issue template, you will be able to provide additional information on the proposed announcement. As a general guide, below the team has outlined three levels for announcements based on the type of announcements and suggested communications activities associated with each tier. The PR team will assess your request in the issue and determine how to proceed. If you are requesting a joint announcement and you are not part of the [Partner Marketing team](/handbook/marketing/strategic-marketing/partner-marketing/), please ensure you ping them on your issue. 

- **Level 1** - A level 1 announcement will be announced via a press release and amplified with social media and an optional blog post. The execution of a blog post will be determined by the blog editorial team on a case by case basis. If the editorial team agrees to have a blog, then the social amplification will be the blog link as it includes assets that are helpful in the link cards across social channels. If there isn't an associated blog post, the social amplification can be for the press release link or relevant news coverage of the announcement. (In this case, the DRI needs to ensure there is an associated image to use with the release link or would make the decision of which news outlet link to select for social amplification.) Example announcements and news include but are not limited to: major GitLab company news around funding, earnings, executive new hires, analyst firm industry awards, acquisitions/mergers, Commit announcements, major joint partner news (ex. a partner such as AWS or Google) and major customer announcement (ex. enterprise or government agencies).
- **Level 2** - A level 2 announcement will be announced via a blog post and amplified with social media. The DRI/SME of the announcement will be responsible for working with the blog editorial team on creating the content and MR for the blog post (please see [the blog handbook](/handbook/marketing/blog/index.html#process-for-time-sensitive-posts) for more on this process). Example announcements and news include but are not limited to: partner integrations, new feature/capability highlights from the monthly release cycles (ex. Windows Shared Runners), customer case study announcement (not household names).
- **Level 3** - A level 3 announcement will be announced and promoted via GitLab’s social media channels. Example announcements and news include but are not limited to: awards from media publications (ex. DEVIES), speaking opps that GitLab employees are participating in (drive attendees/awareness) and ecosystem partner integrations.
- **Other** - In some cases, the following communications channels may be more appropriate:
    - A targeted email to affected users
    - Including an item in an upcoming release post (where the announcement is specifically tied to a release, does not require communication in advance of the release, and is not a sensitive topic)
    - A public issue (see [below](#using-public-issues-to-communicate-with-users))

#### Partner Requests for Press Release Support
If a channel or alliances partner has requested support of their press release with a quote from GitLab, please see the [Channel Marketing handbook](https://about.gitlab.com/handbook/marketing/strategic-marketing/analyst-relations/channel-marketing/) for more details on the approval process, press release templates and partner announcement issue template.  

#### GitLab Press Releases

GitLab's corporate communications team is the [DRI](/handbook/people-group/directly-responsible-individuals/) (directly responsible individual) for writing all press releases that are issued by the company and routing through the appropriate approval process. The team has developed an [issue template to help make the press release development and approval process](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=press-release-template) more streamlined and consistent.  If you have any questions on the press release process or how to make an announcement request, please reach out via the `#external-comms` Slack channel or submit an `announcement` [issue template in the Corporate Marketing project](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=announcement) (see Requests for Announcements section above).

#### Using Public Issues to Communicate with Users

If you would like to [use a GitLab public issue to communicate an update](#using-public-issues-to-communicate-with-users) to customers/users or gather feedback from customers/users, please complete the `announcement` [issue template](/handbook/marketing/corporate-marketing/#requests-for-announcements) with the details of the specific request and draft of the issue announcement copy. The PR team will review the request details and issue copy, as well as recommend other relevant teams (product, product marketing, legal, etc) to loop in for review before posting the public issue.

In some cases it may be more appropriate and [efficient](/handbook/values/#boring-solutions) to communicate with users in a public issue. Below are some examples of the types of communications that may suit an issue as opposed to a blog post, press release, or email, for example:

- Soliciting feedback (e.g. gathering community input on a proposal)
- Explaining upcoming or ongoing changes to GitLab (**not** breaking changes, changes which require users to take action, or otherwise sensitive)
- Sharing updates on a live, ongoing incident (this would be owned by the [Communications Manager on Call](/handbook/engineering/infrastructure/incident-management/#communications-manager-on-call-cmoc-responsibilities))
- See [below](#creating-your-communication-issue) for some examples

Using a public issue has a few advantages:

- In cases where you are seeking feedback or input from the community, your issue can serve as the single source of truth for communication on the subject, rather than spreading communication across a blog post and another page.
- You can easily add related epics and issues so that users can browse all the relevant information and contribute to the discussion. This also keeps the conversation all on GitLab, rather than spreading it across different channels.
- There is one fewer step/link to click on for community members to share their feedback.
- When seeking feedback, keeping things in an issue reinforces that a final decision has not yet been made. A blog post can give the impression of being a formal announcement rather than a proposal, which can have a negative impact on brand sentiment.
- You can get your message out there more quickly, because for most team members creating an issue is more familiar than writing and formatting a blog post. There's also no need to coordinate with the Editorial teams. However, please follow the guidelines in the [PR review and media guidelines](#public-gitlab-issues) section for review from the Corp Comms team.

##### Creating your communication issue

Below are some examples of using an issue to communicate something with GitLab's audience. Feel free to use these examples as a template for your issue. See below [PR review and media guidelines section](#public-gitlab-issues) for more details on the review process.

- [Feedback for ending support for Internet Explorer 11](https://gitlab.com/gitlab-org/gitlab/issues/197987)
- [Recent changes in GitLab routing](https://gitlab.com/gitlab-org/gitlab/-/issues/214217)
- [Color updates in GitLab to establish an accessible baseline](https://gitlab.com/gitlab-org/gitlab/-/issues/212881)

At a minimum, your issue should:

- **Be created in the most relevant GitLab project.** This would likely be the project where most of the discussion or work on your initiative is being done.
- **Have the `user communication` label applied.**
- **Include context for your message.** What background information might someone need to know to understand what you are communicating or asking? What relevant issues or epics can you add to help people understand?
- **Use subheadings for structure.** This makes your issue easy for readers to skim and pick out the most relevant information to them.
- **Include any key dates.** Be sure to call out if there is a deadline for submitting feedback.

##### Promoting a Public Issue

We can spread the word about a public issue in the same way that we would promote a blog post:
- You can [request promotion of your item on GitLab's social channels](/handbook/marketing/corporate-marketing/social-marketing/#social-medias-place-in-an-integrated-marketing-and-communications-strategy).
- You can request that your item be included in an upcoming newsletter by leaving a comment on the appropriate [newsletter issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues?label_name%5B%5D=Newsletter).
- Ask in #marketing on Slack if you would like to promote in some other way.

We can promote other items this way as well: for example surveys, landing pages, or handbook pages.

#### Best Practices for an Announcement and Understanding Media Embargoes

GitLab team members will find [embargo and announcement guidelines in a confidential issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/3348) (_must be logged in to access_).

This answers the following questions.

1. What is an embargo?
1. Why do we set embargoes?
1. Why don’t we talk about embargoed news prior to the announcement?
1. Who do we share embargoed news with?
1. Who is it okay to discuss this news with?
1. When can I talk about news freely with anyone?

## Media/Speaking Guidelines and Training

Speaking on behalf of GitLab via a public channel such as a media interview (in-person or via phone), a podcast, a public issue on GitLab, a forum, a conference/event (live or virtual), a blog or an external platform is a significant responsibility. If you are unsure whether or not you should accept a speaking opportunity, create a public issue to gather feedback/communicate a message, or provide comment representing GitLab to a member of the media or influencer, please see below for guidance.

### Speaking Opportunities

If you are asked to speak on behalf of GitLab, consider reaching out to the PR and Developer Evangelist teams to ensure that the opportunity aligns with GitLab objectives. Inquiries should be initiated in the `#external-comms` Slack channel.

You may be approached by external parties seeking to provide payment for a GitLab team member's time to discuss GitLab remote practice to help them guide a client. We ask that team members verify who they are speaking with to make sure the source is indeed valid and the request legitimate. Remember that you represent GitLab and if any question makes you uncomfortable or gives you a pause on whether you should answer, we recommend that you do not answer. Questions regarding GitLab financials, sales, compliance, executives or where the company is heading should be treated with caution, as should anything which is not already available in the GitLab handbook. We  encourage all team members to be [remote evangelists](/handbook/marketing/corporate-marketing/all-remote/#evangelism-materials) and this can be done without sharing [Not Public](/handbook/communication/#not-public) information about GitLab.

### Research-oriented requests

For analyst research-oriented requests, please consult the [Analyst Relations](/handbook/marketing/strategic-marketing/analyst-relations/) handbook section and direct questions to the `#analyst-relations` Slack channel.

For press/media inquiries which are more research oriented, please share the request and associated context in the `#external-comms` Slack channel. An example of a research-oriented request is an invitation to participate in an interview to share more about one's day-to-day role at GitLab, specific tools teams use on a daily basis, how a team interacts and collaborates with other teams, specific products that are most helpful to them in their workflow, etc. The corporate marketing team will evaluate for reach, awareness, merit, and potential [conflicts of interest](/handbook/legal/gitlab-code-of-business-conduct-and-ethics/#iv-conflicts-of-interest), and advise on a case-by-case basis.

### Honorariums

If the a research-oriented request or speaking opportunity has an honorarium offered (for example, a gift card or cash amount awarded for participation), please go through our [Approvals for Outside Projects & Activities process](/handbook/people-group/contracts-probation-periods/#approval-for-outside-projects--activities-aka-exceptions-to-piaa) to ensure a [conflict of interest is not present](/handbook/legal/gitlab-code-of-business-conduct-and-ethics/#iv-conflicts-of-interest).

### Media mentions, incoming media requests and interviews

As with any growing company in the public eye, media coverage is very common. GitLab's transparent nature has led to some very positive media coverage and community engagement on social channels and message boards; however, it has also at times resulted in negative criticism of the company - both valid and in-valid. This is very common and is expected to continue as the company continues to mature. To stay up-to-date on media coverage, flag any articles or podcasts you see or ask questions, please post in the #newswire Slack channel.

Due to GitLab's unique company transparency and mission that everyone can contribute, team members may be approached by reporters, podcasts hosts, etc. to comment on the company and/or conduct interviews. If you are asked to be quoted or to provide commentary on any matter as a spokesperson of GitLab, please provide detail of the opportunity to the PR team in the `#external-comms` Slack channel before engaging.

In the event that a media member, editor, or publisher offers a draft or preview of an article where you are quoted, please allow the PR team to review by posting in the `#external-comms` Slack channel. The PR team will ensure that the appropriate GitLab team member(s) review and approve in a timely manner.

### Public GitLab Issues

If you would like to [use a GitLab public issue to communicate an update](#using-public-issues-to-communicate-with-users) to customers/users or gather feedback from customers/users, please complete the `announcement` [issue template](/handbook/marketing/corporate-marketing/#requests-for-announcements) with the details of the specific request and draft of the issue announcement copy. The PR team will review the request details and issue copy, as well as recommend other relevant teams (product, product marketing, legal, etc) to loop in for review before posting.

### Social media

Please consult the [Social Marketing Handbook](/handbook/marketing/corporate-marketing/social-marketing/). If you are contacted on a social media platform and asked to share/retweet or provide commentary as a spokesperson of GitLab, feel welcome to provide detail of the opportunity to the social team in the `#social-media` Slack channel.

### Writing about GitLab on your personal blog or for external platforms

You are welcome to write about your experience as a GitLab team member on your personal blog or for other publications and you do not need permission to do so. If you would like someone to check your draft before submitting, you can share it with the PR team who will be happy to review. Please post it in the `#external-comms` Slack channel with a short summary of what your blog post 

## GitLab Press Page and Press Kit

### Updating the Press Page

#### How to Add a New Press Release

1. Create a new merge request and branch in www-gitlab-com.
1. On your branch, navigate to `source` then `press` and click on the [`releases` folder](https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/source/press/releases).
1. Add a new file using the following format `YYYY-MM-DD-title-of-press-release.html.md`.
1. Add the following to the beginning of your document:

```
---
layout: markdown_page
title: "Title of press release"
description: "short sentence overview of press release'
twitter_image: "/location/of/image.png"
twitter_creator: "@gitlab"
twitter_site: "@gitlab"
twitter_image_alt: "Celebrating GitLab's press release about xx with fun emojis"
---
```

`description`, `twitter_image`, and `twitter_image_alt` would be the three, net-new tags needed to be custom to every single release. `twitter_creator` and `twitter_site` would be static with the value "@gitlab".

1. Add the content of the press release to the file and save. Make sure to include any links. It is important to not have any extra spaces after sentences that end a paragraph or your pipeline will break. You must also not have extra empty lines at the end of your doc. So make sure to check that when copying and pasting a press release from a google doc.
1. Assign @wspillane to the MR to add the social sharing image to the `twitter_image:` section above and to merge the release. If the team has previously discussed the work, the social team may have already created a sharing image. If not, they will need to create it from this mention. The social team will add the data necessary to the MR for social sharing and merge the press release.

#### Updating the `press/#press-releases` Page

When you have added a press release, be sure to update the index page too so that it is linked to from [/press/#press-releases](/press/#press-releases).

1. On the same branch, navigate to `data` then to the [`press_releases.yml` file](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/press_releases.yml).
1. Scroll down to `press_releases:`, then scroll to the most recent dated press release.
1. Underneath, add another entry for your new press release using the same format as the others, ensuring that your alignment is correct and that dashes and words begin in the same columns.
1. The URL for your press release will follow the format of your filename for it: `/press/releases/YYYY-MM-DD-title-of-press-release.html`.

#### How to Update the Recent News Page

1. On a regular basis the PR agency teams will send a digest of coverage highlights and news articles.
1. The PR team will update the `Recent News` section with the most recent listed at the top. Display 10 articles at a time. To avoid formatting mistakes, copy and paste a previous entry on the page, and edit with the details of the new coverage. You may need to search online for a thumbnail to upload to `images/press`, if coverage from that publication is not already listed on the page. If you upload a new image, make sure to change the path listed next to `image_tag`.

